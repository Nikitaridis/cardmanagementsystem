
/**
 * Author:  julius
 * Created: 28 Feb 2020
 */


-- Schema: cardmanagement

-- DROP SCHEMA cardmanagement;

CREATE SCHEMA cardmanagement
  AUTHORIZATION cardmanagement;



-- Table: cardmanagement.card

-- DROP TABLE cardmanagement.card;

CREATE TABLE cardmanagement.card
(
  card_id character varying(255) NOT NULL,
  card_number character varying(255) NOT NULL,
  status character varying(255) NOT NULL,
  created_date date NOT NULL,
  activated_date date NOT NULL,
  primary_secondary_flag character varying(255) NOT NULL,
  expiry_date date NOT NULL,
  available_balance character varying(255) NOT NULL,
  CONSTRAINT card_pkey PRIMARY KEY (card_id, card_number)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cardmanagement.card
  OWNER TO cardmanagement;
'';


-- Table: cardmanagement.card_holder

-- DROP TABLE cardmanagement.card_holder;

CREATE TABLE cardmanagement.card_holder
(
  govid character varying(255) NOT NULL,
  date_of_birth character varying(255),
  name character varying(255),
  surname character varying(255),
  title character varying(255),
  CONSTRAINT card_holder_pkey PRIMARY KEY (govid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cardmanagement.card_holder
  OWNER TO cardmanagement;



-- Table: cardmanagement.card_holder_cards

-- DROP TABLE cardmanagement.card_holder_cards;

CREATE TABLE cardmanagement.card_holder_cards
(
  card_card_id character varying(255),
  card_holder_gov_id character varying(255),
  id character varying(255),
  secondary_primary_relation character varying(255)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cardmanagement.card_holder_cards
  OWNER TO cardmanagement;

-- Index: cardmanagement.card_holder_cards_idx

-- DROP INDEX cardmanagement.card_holder_cards_idx;

CREATE INDEX card_holder_cards_idx
  ON cardmanagement.card_holder_cards
  USING btree
  (id COLLATE pg_catalog."default");



-- Table: cardmanagement.card_status_history

-- DROP TABLE cardmanagement.card_status_history;

CREATE TABLE cardmanagement.card_status_history
(
  card_id character varying(255),
  card_status character varying(255),
  "timestamp" timestamp without time zone NOT NULL,
  CONSTRAINT card_status_history_pkey PRIMARY KEY ("timestamp")
)
WITH (
  OIDS=FALSE
);
ALTER TABLE cardmanagement.card_status_history
  OWNER TO cardmanagement;











/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardmanagement.ejb;


import cardmanagement.entity.Card;
import cardmanagement.entity.CardStatusEnum;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author julius
 */
@Stateless
public class CardFacade extends AbstractFacade<Card> {
    
   

    @PersistenceContext(unitName = "CardManagement-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public CardFacade() {
        super(Card.class);
    }
    
    
     /*
        Method that will create cards.
        
    */
    @Override
     public void create(Card entity)throws Exception {
         //CardId will be system generated.
         String cardId = UUID.randomUUID().toString();
         entity.setCardId(cardId);
         
         //created date should be system generated, now ()
         entity.setCreatedDate(new Date());
         
         //new cards must always be created in the status of INACTIVE
         //entity.setStatus(CardStatusEnum.INACTIVE);
         entity.setStatus("INACTIVE");
         
         //check that the length of the card number is 16
         if(entity.getCardNumber().length()!=16  /*!entity.getCardPK().getCardId().toString().isNumeric*/) {
             // try {
                 throw new Exception("Card ID is not 16 characters");
//             } catch (Exception ex) {
//                 Logger.getLogger(CardFacade.class.getName()).log(Level.SEVERE, null, ex); //TODO change error handling 
//             }
         }
          
          getEntityManager().persist(entity);
    }
    
    
   
    
    
    //change the status of a card 
    public void changeCardStatus(String cardId, String newSatus, String currentStatus) throws Exception {
        
        //TODO should make this an anum
        ArrayList<String> validStates = new ArrayList<String>() {{
            add("INACTIVE");
            add("CLOSTED");
            add("LOST");
            add("ACTIVE");
            add("STOLEN");
        }};
        
        if(!validStates.contains(newSatus) || !validStates.contains(currentStatus)){
            throw new Exception("INVALID CARD SATUS ");
        }
        
        if(newSatus.equalsIgnoreCase("INACTIVE")) {
            throw new Exception("card can not be set to INACTIVE once created/ assigned to card holder");
         }
        
         if(newSatus.equalsIgnoreCase("CLOSED")) {
            throw new Exception("card status can not be changed once card has been set to CLOSED");
         }
        
        Query cardStatusHistory = em.createNativeQuery("insert into cardManagement.card_status_history values('"+cardId+"','"+currentStatus+"',now())");
        cardStatusHistory.executeUpdate();
        
        //update existing card in the DB 
        Query updateCardSratus = em.createNativeQuery("update cardmanagement.card set status = '"+newSatus+"' where card_id = '"+cardId+"'");
        updateCardSratus.executeUpdate();
        
    }
    

    
}

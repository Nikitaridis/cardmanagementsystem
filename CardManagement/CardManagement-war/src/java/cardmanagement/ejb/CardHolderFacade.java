/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardmanagement.ejb;

import cardmanagement.entity.CardHolder;
import java.util.List;
import java.util.UUID;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author julius
 */
@Stateless
public class CardHolderFacade extends AbstractFacade<CardHolder> {

    @PersistenceContext(unitName = "CardManagement-warPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    
    
    public CardHolderFacade() {
        super(CardHolder.class);
    }
    
    //method that will create a cardHolder.
    @Override
    public void create(CardHolder cardHolder) {
        getEntityManager().persist(cardHolder);
        //now that the card holder has been created, the status of the primary card that has been assigned should be set to active 
        //in the cards table 
        Query updateCardQuery = em.createNativeQuery("update cardmanagement.card set status = 'ACTIVE' where card_id = '"+cardHolder.getPrimaryCardId()+"'");
        updateCardQuery.executeUpdate();
    }
    
    
    
  
    public void assignCard(String cardHolderId, String newCardId) throws Exception {
        Query getNewCard = em.createNativeQuery("select status from card where card_id='"+newCardId+"'");
        String status = (String)getNewCard.getParameterValue("ans");
        
        if(!status.equalsIgnoreCase("INACTIVE")) {
            throw new Exception("Card not in valid state to be assogned to card holder");
}
        
        Query createPrimaryCard= em.createNativeQuery("insert into cardmanagement.card_holder_cards values('"+newCardId+"','"
                                    +cardHolderId+"','"+UUID.randomUUID()+"','SECONDARY')");
        
        createPrimaryCard.executeUpdate();
    }
    
    
    //method that will replace cards for a card holder
    public void replaceCard(String oldCardNumber, String newCardNumber, String cardHolderId) {
        Query updateQuery = em.createNativeQuery("update card_holder_cards set card_card_id ='"+newCardNumber+"' where card_card_id = '"+oldCardNumber+"'"
                + " and card_holder_gov_id = '"+cardHolderId+"'");
        updateQuery.executeUpdate();
        
    }
    
    
    
    
    //method for adding a secondary card
    public void addSecondaryCard(String cardHolderId,String secondaryCardId) throws Exception {
        if(numberOfSecondaryCards(cardHolderId) > 5) {
            throw new Exception ("Card holder already has maximum secondary cards assigned");
        }
        Query insertSecondaryQuery = em.createNativeQuery("insert into cardmanagement.card_holder_cards values('"+secondaryCardId+"','"
                                    +cardHolderId+"','"
                                    +UUID.randomUUID()+"','SECONDARY')");
        insertSecondaryQuery.executeUpdate();
    }
    
    
    
    
    //validate that card holder does not have more than 6 secondary cards
    public int numberOfSecondaryCards(String cardHolderId) {
        Query validateSecondaryCards = em.createNativeQuery("select count (*) as ans from cardmanagement.card_holder_cards where card_holder_gov_id = '"+cardHolderId+"' and secondary_primary_relation='SECONDARY';");
        int result = (int)validateSecondaryCards.getParameterValue("ans");
        return result;
    }
    
    
    
      //validate that a card holder only has one primary card
     //it is assumed
    public boolean hasPrimaryCards(String cardHolderId) {
        Query validatePrimaryCards = em.createNativeQuery("select count (*) as ans from cardmanagement.card_holder_cards where card_holder_gov_id = '"+cardHolderId+"' and secondary_primary_relation='PRIMARY';");
        int result = (int)validatePrimaryCards.getParameterValue("ans");
        if(result > 1) {
            return false;
        } else {
            return true;
        }
    }
    
   
    //this should return a list of all PRIMARY cards that are in a status of INACTIVE
    public List<String> getAvailableCards() {
        Query getAvailableCards = em.createNativeQuery("select card_id as cardId from cardmanagement.card where status='INACTIVE' and primary_secondary_flag = 'PRIMARY'");
        return getAvailableCards.getResultList();
    } 
    
    
    
    
    public List<String> getAvailableCardsJPA() {
        Query getAvailableCards = em.createQuery("select e from cardmanagement.card where e.status='INACTIVE' and e.primary_secondary_flag = 'PRIMARY'");
        return (List<String>)getAvailableCards.getResultList();
    }
    
}

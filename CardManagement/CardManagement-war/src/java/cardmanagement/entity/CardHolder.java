package cardmanagement.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * * * @author julius
 */
@Entity
@Table(name = "card_holder")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CardHolder.findAll", query = "SELECT c FROM CardHolder c"),
    @NamedQuery(name = "CardHolder.findByTitle", query = "SELECT c FROM CardHolder c WHERE c.title = :title"),
    @NamedQuery(name = "CardHolder.findByName", query = "SELECT c FROM CardHolder c WHERE c.name = :name"),
    @NamedQuery(name = "CardHolder.findBySurname", query = "SELECT c FROM CardHolder c WHERE c.surname = :surname"),
    @NamedQuery(name = "CardHolder.findByDateOfBirth", query = "SELECT c FROM CardHolder c WHERE c.dateOfBirth = :dateOfBirth"),
    @NamedQuery(name = "CardHolder.findByGovid", query = "SELECT c FROM CardHolder c WHERE c.govid = :govid")})
public class CardHolder implements Serializable {

    private static final long serialVersionUID = 1L;
    @Size(max = 255)
    @Column(name = "title")
    private String title;
    @Size(max = 255)
    @Column(name = "name")
    private String name;
    @Size(max = 255)
    @Column(name = "surname")
    private String surname;
    @Size(max = 255)
    @Column(name = "date_of_birth")
    private String dateOfBirth;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "govid")
    private String govid;
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "primary_card_id")
    private String primaryCardId;

    public String getPrimaryCardId() {
        return primaryCardId;
    }

    public void setPrimaryCardId(String primaryCardId) {
        this.primaryCardId = primaryCardId;
    }
   

    public CardHolder() {
    }

    public CardHolder(String govid) {
        this.govid = govid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getGovid() {
        return govid;
    }

    public void setGovid(String govid) {
        this.govid = govid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (govid != null ? govid.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof CardHolder)) {
            return false;
        }
        CardHolder other = (CardHolder) object;
        if ((this.govid == null && other.govid != null) || (this.govid != null && !this.govid.equals(other.govid))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cardmanagement.entity.CardHolder[ govid=" + govid + " ]";
    }
}

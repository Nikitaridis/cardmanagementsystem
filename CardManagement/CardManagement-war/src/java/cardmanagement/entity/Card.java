/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cardmanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author julius
 */
@Entity
@Table(name = "card")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Card.findAll", query = "SELECT c FROM Card c"),
    @NamedQuery(name = "Card.findByCardId", query = "SELECT c FROM Card c WHERE c.cardId = :cardId"),
    @NamedQuery(name = "Card.findByCardNumber", query = "SELECT c FROM Card c WHERE c.cardNumber = :cardNumber"),
    @NamedQuery(name = "Card.findByStatus", query = "SELECT c FROM Card c WHERE c.status = :status"),
    @NamedQuery(name = "Card.findByCreatedDate", query = "SELECT c FROM Card c WHERE c.createdDate = :createdDate"),
    @NamedQuery(name = "Card.findByActivatedDate", query = "SELECT c FROM Card c WHERE c.activatedDate = :activatedDate"),
    @NamedQuery(name = "Card.findByPrimarySecondaryFlag", query = "SELECT c FROM Card c WHERE c.primarySecondaryFlag = :primarySecondaryFlag"),
    @NamedQuery(name = "Card.findByExpiryDate", query = "SELECT c FROM Card c WHERE c.expiryDate = :expiryDate"),
    @NamedQuery(name = "Card.findByAvailableBalance", query = "SELECT c FROM Card c WHERE c.availableBalance = :availableBalance")})
public class Card implements Serializable {

   
    
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = true)
    //@NotNull
    @Size(min = 1, max = 255)
    @Column(name = "card_id")
    private String cardId;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "card_number")
    private String cardNumber;
    
    
//    @Enumerated(EnumType.STRING)
//    private CardStatusEnum status;
    
    @Basic(optional = true)
    @Column(name = "status")
    private String status;
    @Size(min = 1, max = 255)

 
    //@NotNull
    @Column(name = "created_date")
    @Temporal(TemporalType.DATE)
    private Date createdDate;
    @Basic(optional = true)
    
    @NotNull
    @Column(name = "activated_date")
    @Temporal(TemporalType.DATE)
    private Date activatedDate;
    @Basic(optional = false)
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "primary_secondary_flag")
    private String primarySecondaryFlag;
    @Basic(optional = false)
    
    @NotNull
    @Column(name = "expiry_date")
    @Temporal(TemporalType.DATE)
    private Date expiryDate;
    @Basic(optional = false)
    
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "available_balance")
    private String availableBalance;
    

    public Card() {
    }

    public Card(String cardId) {
        this.cardId = cardId;
    }

    public Card(String cardId, String cardNumber, String status, Date createdDate, Date activatedDate, String primarySecondaryFlag, Date expiryDate, String availableBalance) {
        this.cardId = cardId;
        this.cardNumber = cardNumber;
        this.status = status;
        this.createdDate = createdDate;
        this.activatedDate = activatedDate;
        this.primarySecondaryFlag = primarySecondaryFlag;
        this.expiryDate = expiryDate;
        this.availableBalance = availableBalance;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getActivatedDate() {
        return activatedDate;
    }

    public void setActivatedDate(Date activatedDate) {
        this.activatedDate = activatedDate;
    }

    public String getPrimarySecondaryFlag() {
        return primarySecondaryFlag;
    }

    public void setPrimarySecondaryFlag(String primarySecondaryFlag) {
        this.primarySecondaryFlag = primarySecondaryFlag;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(String availableBalance) {
        this.availableBalance = availableBalance;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cardId != null ? cardId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Card)) {
            return false;
        }
        Card other = (Card) object;
        if ((this.cardId == null && other.cardId != null) || (this.cardId != null && !this.cardId.equals(other.cardId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "cardmanagement.entity.Card[ cardId=" + cardId + " ]";
    }
    
}
